# frozen_string_literal: true

require_relative '../triage'

module Triage
  module Reaction
    module_function

    def add_comment(body, noteable_path = event.noteable_path)
      path = "#{noteable_path}/notes"

      Reaction.post_request(path, body)
    end

    def add_discussion(body, noteable_path = event.noteable_path)
      path = "#{noteable_path}/discussions"

      Reaction.post_request(path, body)
    end

    def append_discussion(body, discussion_id, noteable_path = event.noteable_path)
      path = "#{noteable_path}/discussions/#{discussion_id}/notes"

      Reaction.post_request(path, body)
    end

    def resolve_discussion(discussion_id, noteable_path = event.noteable_path)
      path = "#{noteable_path}/discussions/#{discussion_id}"

      Reaction.put_request(path, resolved: true)
    end

    def unresolve_discussion(discussion_id, noteable_path = event.noteable_path)
      path = "#{noteable_path}/discussions/#{discussion_id}"

      Reaction.put_request(path, resolved: false)
    end

    def self.post_request(path, body)
      Triage.api_client.post(path, body: { body: body }) unless Triage.dry_run?

      "POST #{PRODUCTION_API_ENDPOINT}#{path}, body: `#{body}`"
    end

    def self.put_request(path, body)
      Triage.api_client.put(path, body: body) unless Triage.dry_run?

      "PUT #{PRODUCTION_API_ENDPOINT}#{path}, body: `#{body}`"
    end
  end
end
