# frozen_string_literal: true

require 'digest'

require_relative '../../triage/processor'
require_relative '../../triage/rate_limit'

module Triage
  # Allows any community contributor to ask for a review.
  # The automation will pick a random MR coach and ask them to review the MR.
  class CommandMrRequestReview < Processor
    include RateLimit

    WORKFLOW_READY_FOR_REVIEW_LABEL = 'workflow::ready for review'

    react_to 'merge_request.note'
    define_command name: 'ready', aliases: %w[review request_review]

    def applicable?
      event.from_gitlab_org? &&
        event.wider_community_author? &&
        command.valid?(event)
    end

    def process
      comment = <<~MARKDOWN.rstrip
        /label ~"#{WORKFLOW_READY_FOR_REVIEW_LABEL}"
      MARKDOWN

      add_comment comment
    end

    def documentation
      <<~TEXT
        # TODO: Add actual documentation
      TEXT
    end

    private

    def cache_key
      @cache_key ||= Digest::MD5.hexdigest("request_review-commands-sent-#{event.event_actor_id}-#{event.noteable_path}")
    end

    def rate_limit_count
      1
    end

    def rate_limit_period
      86400 # 1 day
    end
  end
end
