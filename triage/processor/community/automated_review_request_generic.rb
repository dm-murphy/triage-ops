# frozen_string_literal: true

require_relative '../../triage/processor'

module Triage
  class AutomatedReviewRequestGeneric < Processor
    WORKFLOW_READY_FOR_REVIEW_LABEL = 'workflow::ready for review'

    react_to 'merge_request.update'

    def applicable?
      event.from_gitlab_org? &&
        event.wider_community_author? &&
        event.resource_open? &&
        workflow_ready_for_review_added? &&
        event.wip?
    end

    def process
      post_review_request_comment
    end

    private

    def workflow_ready_for_review_added?
      event.added_label_names.include?(WORKFLOW_READY_FOR_REVIEW_LABEL)
    end

    def post_review_request_comment
      add_comment('/draft')
    end
  end
end
