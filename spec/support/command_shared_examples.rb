RSpec.shared_examples 'command processor' do |commands|
  let(:args_regex) { nil }

  it 'registers the expected command' do
    expect(subject.command.names).to eq(Array(commands))
  end

  it 'registers the expected args_regex' do
    expect(subject.command.args_regex).to eq(args_regex)
  end
end
