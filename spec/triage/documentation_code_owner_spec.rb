# frozen_string_literal: true

require 'spec_helper'

require_relative '../../triage/triage/documentation_code_owner'

describe Triage::DocumentationCodeOwner do
  let(:project_id) { 123 }
  let(:merge_request_iid) { 888 }
  let(:tech_writer1) { 'marcia' }
  let(:tech_writer2) { 'aqualls' }
  let(:docs_team) { 'gl-docsteam' }

  let(:tech_writer_approval1) do
    {
      rule_type: described_class::RULE_TYPE_CODE_OWNER,
      users: [{ username: tech_writer1 }],
      groups: [],
      section: 'Docs Create'
    }
  end

  let(:tech_writer_approval2) do
    {
      rule_type: described_class::RULE_TYPE_CODE_OWNER,
      users: [{username: tech_writer2}],
      groups: [],
      section: 'Docs Plan'
    }
  end

  let(:docs_team_approval) do
    {
      rule_type: described_class::RULE_TYPE_CODE_OWNER,
      users: [],
      groups: [{ full_path: docs_team }],
      section: 'Documentation'
    }
  end

  subject { described_class.new(project_id, merge_request_iid) }

  before do
    stub_api_request(
      path: "/projects/#{project_id}/merge_requests/#{merge_request_iid}/approval_rules",
      response_body: merge_request_approval_rules)
  end

  describe '#merge_request_approvers' do
    context 'when there are documentation sections' do
      context 'with individual tech writers' do
        let(:merge_request_approval_rules) do
          [
            tech_writer_approval1,
            tech_writer_approval2,
            docs_team_approval
          ]
        end

        it 'returns the technical writers' do
          expect(subject.approvers).to contain_exactly(tech_writer1, tech_writer2)
        end
      end

      context 'without individual technical writers' do
        let(:merge_request_approval_rules) do
          [
            docs_team_approval
          ]
        end

        it 'returns gl-docsteam group' do
          expect(subject.approvers).to contain_exactly(docs_team)
        end
      end

      context 'with both docs team and individual tech writers' do
        let(:merge_request_approval_rules) do
          [
            tech_writer_approval1,
            docs_team_approval
          ]
        end

        it 'returns the technical writers' do
          expect(subject.approvers).to contain_exactly(tech_writer1)
        end
      end

      context 'with repeated individual tech writers' do
        let(:merge_request_approval_rules) do
          [
            tech_writer_approval1,
            tech_writer_approval1,
            docs_team_approval
          ]
        end

        it 'returns the technical writers' do
          expect(subject.approvers).to contain_exactly(tech_writer1)
        end
      end
    end

    context 'when approval rules have no documentation sections' do
      let(:non_docs_approval_user) do
        {
          rule_type: described_class::RULE_TYPE_CODE_OWNER,
          users: [{ username: 'godfat-gitlab' }],
          groups: [],
          section: 'Engineering Productivity'
        }
      end

      let(:non_docs_approval_group) do
        {
          rule_type: described_class::RULE_TYPE_CODE_OWNER,
          users: [],
          groups: [{ full_path: 'gitlab-org/quality/engineering-productivity' }],
          section: 'Engineering Productivity'
        }
      end

      let(:merge_request_approval_rules) do
        [
          non_docs_approval_user,
          non_docs_approval_group
        ]
      end

      it 'returns empty array' do
        expect(subject.approvers).to be_empty
      end
    end

    context 'when approval rules does not contain code owner rules' do
      let(:merge_request_approval_rules) do
        [
          {
            rule_type: 'any_approver',
            users: [],
            groups: []
          }
        ]
      end

      it 'returns empty array' do
        expect(subject.approvers).to be_empty
      end
    end

    context 'when approval rule section is null' do
      let(:merge_request_approval_rules) do
        [
          {
            rule_type: 'code_owner',
            users: [],
            groups: [],
            section: nil,
          }
        ]
      end

      it 'returns empty array' do
        expect(subject.approvers).to be_empty
      end
    end
  end
end
