# frozen_string_literal: true

require 'spec_helper'

require_relative '../../../triage/processor/community/command_mr_label'

RSpec.describe Triage::CommandMrLabel do
  include_context 'with event', 'Triage::NoteEvent' do
    let(:event_attrs) do
      {
        from_gitlab_org?: from_gitlab_org,
        wider_community_author?: wider_community_author,
        by_noteable_author?: by_noteable_author,
        new_comment: %(#{Triage::GITLAB_BOT} label #{labels})
      }
    end

    let(:from_gitlab_org) { true }
    let(:wider_community_author) { true }
    let(:by_noteable_author) { true }
    let(:labels) { '~"group::import"' }
  end

  subject { described_class.new(event) }

  it_behaves_like 'registers listeners', ['issue.note', 'merge_request.note']
  it_behaves_like 'command processor', 'label' do
    let(:args_regex) { described_class::LABELS_REGEX }
  end

  describe 'LABELS_REGEX' do
    it { expect(described_class::LABELS_REGEX).to eq(/~"([^"]+)"|~([^ ]+)/) }
  end

  describe 'ALLOWED_LABELS_REGEX' do
    it { expect(described_class::ALLOWED_LABELS_REGEX).to eq(/\A(type|group)::[^:]+\z/) }
  end

  describe '#applicable?' do
    context 'when event is for a new merge request mentioning the bot' do
      before do
        allow(event).to receive(:note?).and_return(false)
        allow(event).to receive(:new_entity?).and_return(true)
        allow(event).to receive(:merge_request?).and_return(true)
      end

      where(:label) do
        [
          'type::feature',
          'group::import',
          'workflow::in dev',
          'workflow::ready for review'
        ]
      end

      let(:labels) { %Q(~"#{label}") }

      with_them do
        it_behaves_like 'event is applicable'
      end
    end

    context 'when event project is not under gitlab-org' do
      let(:from_gitlab_org) { false }

      include_examples 'event is not applicable'
    end

    context 'when author is not from the wider community' do
      let(:wider_community_author) { false }

      include_examples 'event is not applicable'
    end

    context 'when the comment is not from the resource author' do
      let(:by_noteable_author) { false }

      include_examples 'event is not applicable'
    end

    context 'when event is for a non-allowed label' do
      context 'when label is ~"pick into x"' do
        let(:labels) { '~"pick into x"' }

        include_examples 'event is not applicable'
      end

      context 'when label is ~"group::import::nested"' do
        let(:labels) { '~"group::import::nested"' }

        include_examples 'event is not applicable'
      end

      context 'when label is ~"workflow::in review"' do
        let(:labels) { '~"workflow::in review"' }

        include_examples 'event is not applicable'
      end
    end
  end

  describe '#documentation' do
    it_behaves_like 'processor documentation is present'
  end

  describe '#process' do
    shared_examples 'message posting' do |expected_labels: nil, response_body: {}, response_status: 200, expected_error: nil|
      it 'posts /label command' do
        body = <<~MARKDOWN.chomp
          /label #{expected_labels || labels}
        MARKDOWN

        expect_comment_request(event: event, body: body, response_body: response_body, response_status: response_status) do
          if expected_error
            expect { subject.process }.to raise_error(expected_error)
          else
            expect { subject.process }.not_to raise_error
          end
        end
      end
    end

    context 'when event is for a new issue mentioning the bot' do
      before do
        allow(event).to receive(:note?).and_return(false)
        allow(event).to receive(:new_entity?).and_return(true)
        allow(event).to receive(:issue?).and_return(true)
      end

      context 'when label is ~"group::import"' do
        it_behaves_like 'message posting'
      end

      context 'when command is not at the beginning of the new_comment' do
        before do
          allow(event).to receive(:new_comment).and_return(%(Hello\n#{Triage::GITLAB_BOT} label #{labels}\nWorld!))
        end

        it_behaves_like 'message posting'

        context 'with multiple labels on the same line with extra spaces' do
          let(:labels) { '  ~"group::import"   ~"group::source code"   ' }

          it_behaves_like 'message posting', expected_labels: '~"group::import" ~"group::source code"'
        end

        context 'with multiple commands on separate lines' do
          before do
            allow(event).to receive(:new_comment).and_return(%(Hello\n#{Triage::GITLAB_BOT} label #{labels}\nWorld!\n#{Triage::GITLAB_BOT} label ~group::foo))
          end

          it_behaves_like 'message posting', expected_labels: '~"group::import"'
        end
      end

      context 'when command tries to set an unknown label, and API returns a "note cannot be blank" 400 error' do
        let(:labels) { '~"group::unknown"' }

        it_behaves_like 'message posting', response_body: { message: %(400 Bad request - Note {:note=>["can't be blank"]}) }, response_status: 400
      end

      context 'when command tries to set an unknown label, and API returns another 400 error' do
        let(:labels) { '~"group::unknown"' }

        it_behaves_like 'message posting', response_body: { message: %(400 Bad request - Another error) }, response_status: 400, expected_error: Gitlab::Error::BadRequest
      end
    end

    it_behaves_like 'rate limited'
  end
end
