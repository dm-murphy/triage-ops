# frozen_string_literal: true

require 'spec_helper'

require_relative '../../../triage/processor/community/thank_contribution'

RSpec.describe Triage::ThankContribution do
  include_context 'with event', 'Triage::MergeRequestEvent' do
    let(:event_attrs) do
      {
        from_gitlab_org?: from_gitlab_org,
        wider_community_author?: wider_community_author,
        event_actor_username: 'root',
        project_id: project_id
      }
    end
    let(:from_gitlab_org) { true }
    let(:wider_community_author) { true }
    let(:project_id) { 13_579 }
  end

  subject { described_class.new(event) }

  include_examples 'registers listeners', ['merge_request.open']

  describe '#applicable?' do
    include_examples 'event is applicable'

    context 'when event project is not under gitlab-org' do
      let(:from_gitlab_org) { false }

      include_examples 'event is not applicable'
    end

    context 'when author is not from the wider community' do
      let(:wider_community_author) { false }

      include_examples 'event is not applicable'
    end

    context 'for project with custom conditions' do
      let(:project_id) { described_class::WWW_GITLAB_COM_PROJECT_ID }

      before do
        allow(event).to receive(:from_gitlab_com?).and_return(false)
      end

      it 'processes different conditions' do
        expect(event).to receive(:from_gitlab_com?)
        subject.applicable?
      end
    end
  end

  describe '#documentation' do
    it_behaves_like 'processor documentation is present'
  end

  describe '#process' do
    let(:expected_message_template) { Triage::Strings::Thanks::DEFAULT_THANKS }
    let(:expected_message) do format(expected_message_template, author_username: event_attrs[:event_actor_username])
    end

    it 'posts a default message' do
      expect_comment_request(event: event, body: expected_message) do
        subject.process
      end
    end

    context 'when project_id has override thanks' do
      let(:expected_message_template) { described_class::PROJECT_THANKS[project_id][:message] }

      context 'GitLab' do
        let(:project_id) { 278_964 }
        let(:expected_message) do
          <<~MARKDOWN.chomp
            :wave: @root

            Thank you for your contribution to GitLab. We believe that [everyone can contribute](https://about.gitlab.com/company/mission/#mission)
            and contributions like yours are what make GitLab great!

            * Our [Merge Request Coaches](https://about.gitlab.com/company/team/?department=merge-request-coach)
            will ensure your contribution is reviewed in a timely manner[*](https://about.gitlab.com/handbook/engineering/quality/merge-request-triage).
            * If you haven't, please set up a [`DANGER_GITLAB_API_TOKEN`](https://docs.gitlab.com/ee/development/dangerbot.html#limitations).
            * You can comment `@gitlab-bot label ~"group::<group name>"` to add a [group label](https://docs.gitlab.com/ee/development/contributing/issue_workflow.html#group-labels) or `@gitlab-bot label ~"type::<type name>"` to add a [type label](https://about.gitlab.com/handbook/engineering/metrics/#work-type-classification).
            * When you're ready for a first review, just post `@gitlab-bot request_review`, and we'll take care of the rest.
            * If you need help moving the merge request forward, feel free to post `@gitlab-bot help`.
            * Read more on [how to get help](https://about.gitlab.com/community/contribute/#getting-help).

            *This message was [generated automatically](https://about.gitlab.com/handbook/engineering/quality/triage-operations/#ensure-quick-feedback-for-community-contributions).
            You're welcome to [improve it](https://gitlab.com/gitlab-org/quality/triage-ops/-/blob/master/triage/strings/thanks.rb).*

            /label ~"Community contribution" ~"workflow::in dev"
            /assign @root
          MARKDOWN
        end

        it 'posts custom project comment' do
          expect_comment_request(event: event, body: expected_message) do
            subject.process
          end
        end
      end

      context 'runner' do
        let(:project_id) { 250_833 }

        it 'posts custom project comment' do
          expect_comment_request(event: event, body: expected_message) do
            subject.process
          end
        end
      end

      context 'website' do
        let(:project_id) { 7764 }

        it 'posts custom project comment' do
          expect_comment_request(event: event, body: expected_message) do
            subject.process
          end
        end
      end
    end
  end
end
