# frozen_string_literal: true

require 'spec_helper'

require_relative '../../../triage/processor/community/command_mr_request_review'

RSpec.describe Triage::CommandMrRequestReview do
  include_context 'with event', 'Triage::NoteEvent' do
    let(:event_attrs) do
      {
        from_gitlab_org?: from_gitlab_org,
        wider_community_author?: wider_community_author,
        new_comment: %(@gitlab-bot request_review)
      }
    end

    let(:from_gitlab_org) { true }
    let(:wider_community_author) { true }
    let(:labels) { '~"group::import"' }
  end

  subject { described_class.new(event) }

  it_behaves_like 'registers listeners', ['merge_request.note']
  it_behaves_like 'command processor', %w[ready review request_review]

  describe '#applicable?' do
    include_examples 'event is applicable'

    context 'when event project is not under gitlab-org' do
      let(:from_gitlab_org) { false }

      include_examples 'event is not applicable'
    end

    context 'when author is not from the wider community' do
      let(:wider_community_author) { false }

      include_examples 'event is not applicable'
    end
  end

  describe '#documentation' do
    it_behaves_like 'processor documentation is present'
  end

  describe '#process' do
    let(:body) do
      <<~MARKDOWN.chomp
        /label ~"#{described_class::WORKFLOW_READY_FOR_REVIEW_LABEL}"
      MARKDOWN
    end

    it 'sets ~"workflow::ready for review"' do
      expect_comment_request(event: event, body: body) do
        subject.process
      end
    end

    it_behaves_like 'rate limited', count: 1, period: 86_400
  end
end
