# frozen_string_literal: true

require 'spec_helper'

require_relative '../../../triage/processor/community/command_mr_feedback'

RSpec.describe Triage::CommandMrFeedback do
  include_context 'slack posting context'
  include_context 'with event', 'Triage::NoteEvent' do
    let(:event_attrs) do
      {
        from_gitlab_org?: from_gitlab_org,
        wider_community_author?: wider_community_author,
        by_noteable_author?: by_noteable_author,
        new_comment: %(@gitlab-bot feedback),
        url: 'https://comment.url/note#123'
      }
    end

    let(:from_gitlab_org) { true }
    let(:wider_community_author) { true }
    let(:by_noteable_author) { true }
  end

  subject { described_class.new(event, messenger: messenger_stub) }

  before do
    allow(messenger_stub).to receive(:ping)
  end

  it_behaves_like 'registers listeners', ['merge_request.note']
  it_behaves_like 'command processor', 'feedback'
  it_behaves_like 'processor slack options', '#mr-feedback'

  describe '#applicable?' do
    include_examples 'event is applicable'

    context 'when event project is not under gitlab-org' do
      let(:from_gitlab_org) { false }

      include_examples 'event is not applicable'
    end

    context 'when author is not from the wider community' do
      let(:wider_community_author) { false }

      include_examples 'event is not applicable'
    end

    context 'when the comment is not from the resource author' do
      let(:by_noteable_author) { false }

      include_examples 'event is not applicable'
    end
  end

  describe '#documentation' do
    it_behaves_like 'processor documentation is present'
  end

  describe '#process' do
    it_behaves_like 'slack message posting' do
      let(:message_body) do
        <<~MARKDOWN
          Hola MR coaches, a contributor has left feedback about their experience in #{event.url}.
        MARKDOWN
      end
    end

    it_behaves_like 'rate limited Slack message posting', count: 1, period: 86400
  end
end
