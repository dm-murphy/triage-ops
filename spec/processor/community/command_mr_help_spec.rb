# frozen_string_literal: true

require 'spec_helper'

require_relative '../../../triage/processor/community/command_mr_help'

RSpec.describe Triage::CommandMrHelp do
  include_context 'with event', 'Triage::NoteEvent' do
    let(:event_attrs) do
      {
        from_gitlab_org?: from_gitlab_org,
        wider_community_author?: wider_community_author,
        by_noteable_author?: by_noteable_author,
        new_comment: %(@gitlab-bot help)
      }
    end

    let(:from_gitlab_org) { true }
    let(:wider_community_author) { true }
    let(:by_noteable_author) { true }
  end

  let(:roulette) do
    [
      { 'username' => 'coach', 'merge_request_coach' => true, 'available' => true },
      { 'username' => 'unavailable_coach', 'merge_request_coach' => true, 'available' => false },
      { 'username' => 'not_coach', 'merge_request_coach' => false, 'available' => true }
    ]
  end

  before do
    allow(WwwGitLabCom).to receive(:roulette).and_return(roulette)
  end

  subject { described_class.new(event) }

  it_behaves_like 'registers listeners', ['merge_request.note']
  it_behaves_like 'command processor', 'help'

  describe '#applicable?' do
    include_examples 'event is applicable'

    context 'when event project is not under gitlab-org' do
      let(:from_gitlab_org) { false }

      include_examples 'event is not applicable'
    end

    context 'when author is not from the wider community' do
      let(:wider_community_author) { false }

      include_examples 'event is not applicable'
    end
  end

  describe '#documentation' do
    it_behaves_like 'processor documentation is present'
  end

  describe '#process' do
    it 'posts a comment to reply to author and pings MR coaches' do
      body = <<~MARKDOWN.chomp
        Hey there @coach, could you please help @root out?
        /assign_reviewer @coach
      MARKDOWN

      expect_comment_request(event: event, body: body) do
        subject.process
      end
    end

    it_behaves_like 'rate limited', count: 1, period: 86400
  end
end
