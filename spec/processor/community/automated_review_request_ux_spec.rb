# frozen_string_literal: true

require 'spec_helper'

require_relative '../../../triage/processor/community/automated_review_request_ux'

RSpec.describe Triage::AutomatedReviewRequestUx do
  include_context 'slack posting context'
  include_context 'with event', 'Triage::MergeRequestEvent' do
    let(:event_attrs) do
      {
        from_gitlab_org?: from_gitlab_org,
        wider_community_author?: wider_community_author,
        resource_open?: resource_open,
        project_id: project_id,
        iid: merge_request_iid,
        wip?: false,
        url: url,
        title: title
      }
    end
    let(:from_gitlab_org) { true }
    let(:wider_community_author) { true }
    let(:resource_open) { true }
    let(:project_id) { 123 }
    let(:merge_request_iid) { 300 }
    let(:url) { 'http://gitlab.com/mr_url' }
    let(:title) { 'Merge request title' }
    let(:label_names) { [described_class::UX_LABEL] }
    let(:added_label_names) { [Triage::AutomatedReviewRequestGeneric::WORKFLOW_READY_FOR_REVIEW_LABEL] }
  end

  subject { described_class.new(event, messenger: messenger_stub) }

  before do
    stub_api_request(
      path: "/projects/#{event.project_id}/#{event.object_kind}s/#{event.iid}/notes",
      query: { per_page: 100 },
      response_body: [])
  end

  include_examples 'registers listeners', ["merge_request.update"]

  it_behaves_like 'processor slack options', '#ux-community-contributions'

  describe '#applicable?' do
    before do
      allow(subject.__send__(:unique_comment)).to receive(:no_previous_comment?).and_return(true)
    end

    include_examples 'event is applicable'

    context 'when event project is not under gitlab-org' do
      let(:from_gitlab_org) { false }

      include_examples 'event is not applicable'
    end

    context 'when author is not from the wider community' do
      let(:wider_community_author) { false }

      include_examples 'event is not applicable'
    end

    context 'when resource is not open' do
      let(:resource_open) { false }

      include_examples 'event is not applicable'
    end

    context 'when UX is not set' do
      let(:label_names) { ['bug'] }

      include_examples 'event is not applicable'
    end

    context 'when "workflow::ready for review" is not added' do
      let(:added_label_names) { ['bug'] }

      include_examples 'event is not applicable'
    end

    context 'when docs review was already requested' do
      before do
        allow(subject.__send__(:unique_comment)).to receive(:no_previous_comment?).and_return(false)
      end

      include_examples 'event is not applicable'
    end
  end

  describe '#documentation' do
    it_behaves_like 'processor documentation is present'
  end

  describe '#process' do
    before do
      allow(messenger_stub).to receive(:ping)
    end

    shared_examples 'process UX merge request' do
      it 'posts a comment' do
        body = <<~MARKDOWN.chomp
          #{subject.__send__(:unique_comment).__send__(:hidden_comment)}
          Thanks for helping us improve the UX of GitLab. Your contribution is appreciated! We have pinged our UX team, so stay tuned for their feedback.
        MARKDOWN

        expect_comment_request(event: event, body: body) do
          subject.process
        end
      end
    end

    it_behaves_like 'process UX merge request'
    it_behaves_like 'slack message posting' do
      before do
        allow(subject).to receive(:post_ux_comment)
      end

      let(:message_body) do
        <<~MARKDOWN
          Hi UX team, a new community contribution (#{title}) requires a UX review: #{url}.
        MARKDOWN
      end
    end
  end
end
