# frozen_string_literal: true

require 'spec_helper'

require_relative '../../../triage/processor/community/hackathon_label'

RSpec.describe Triage::HackathonLabel do
  include_context 'with event', 'Triage::MergeRequestEvent' do
    let(:event_attrs) do
      {
        object_kind: 'merge_request',
        action: 'open',
        from_gitlab_org?: true,
        created_at: described_class::HACKATHON_START_DATE
      }
    end
    let(:label_names) { ['Community contribution'] }
  end

  subject { described_class.new(event) }

  include_examples 'registers listeners', ["merge_request.update", "merge_request.open"]

  describe '#applicable?' do
    before do
      allow(subject.__send__(:unique_comment)).to receive(:no_previous_comment?).and_return(true)
    end

    context 'when Community contribution label added' do
      let(:added_label_names) { ['Community contribution'] }

      context 'in gitlab-org' do
        before do
          allow(event).to receive(:from_gitlab_org?).and_return(true)
          allow(event).to receive(:from_gitlab_com?).and_return(false)
        end

        include_examples 'event is applicable'
      end

      context 'in gitlab-com' do
        before do
          allow(event).to receive(:from_gitlab_org?).and_return(false)
          allow(event).to receive(:from_gitlab_com?).and_return(true)
        end

        include_examples 'event is applicable'
      end

      context 'when ~Hackathon label is present' do
        let(:label_names) { super() + [described_class::HACKATHON_LABEL] }

        include_examples 'event is not applicable'
      end

      context 'when merge request is created before hackathon' do
        before do
          allow(event).to receive(:created_at).and_return(described_class::HACKATHON_START_DATE - 1)
        end

        include_examples 'event is not applicable'
      end

      context 'when merge request is created after hackathon' do
        before do
          allow(event).to receive(:created_at).and_return(described_class::HACKATHON_END_DATE + 1)
        end

        include_examples 'event is not applicable'
      end

      context 'when docs review was already requested' do
        before do
          allow(subject.__send__(:unique_comment)).to receive(:no_previous_comment?).and_return(false)
        end

        include_examples 'event is not applicable'
      end
    end

    context 'when no ~"Community contribution" label set' do
      let(:label_names) { ['foo'] }

      include_examples 'event is not applicable'
    end

    context 'when event is neither from gitlab-org or gitlab-com' do
      let(:added_label_names) { ['Community contribution'] }

      before do
        allow(event).to receive(:from_gitlab_org?).and_return(false)
        allow(event).to receive(:from_gitlab_com?).and_return(false)
      end

      include_examples 'event is not applicable'
    end
  end

  describe '#documentation' do
    it_behaves_like 'processor documentation is present'
  end

  describe '#process' do
    it 'posts a message to label Hackathon' do
      body = <<~MARKDOWN.chomp
        This merge request will be considered [part of](https://gitlab.com/gitlab-com/marketing/community-relations/contributor-program/hackathon/-/issues/65) the quarterly [GitLab Hackathon](https://about.gitlab.com/community/hackathon/) for a chance to win a [prize](https://about.gitlab.com/community/hackathon/#prize).

        Can you make sure this merge request mentions or links to the relevant issue that it's attempting to close?

        Thank you for your contribution!

        /label ~"Hackathon"
      MARKDOWN

      expect_comment_request(event: event, body: body) do
        subject.process
      end
    end
  end
end
