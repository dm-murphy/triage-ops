
# frozen_string_literal: true

require 'spec_helper'
require_relative '../../lib/www_gitlab_com'
require_relative '../../lib/create_editor_helper'

RSpec.describe CreateEditorHelper do
  let(:resource_klass) do
    Struct.new(:labels) do
      include CreateEditorHelper
    end
  end

  let(:label_klass) do
    Struct.new(:name)
  end

  let(:labels) { [] }

  let(:team_from_www) do
    {
      'user1' => { 'departments' => ['Create:Editor BE Team'] },
      'user2' => { 'departments' => ['Create:Editor FE Team'] },
      'user3' => { 'departments' => ['Create:Editor BE Team'] },
      'user4' => { 'departments' => ['Create:Editor FE Team'] }
    }
  end

  subject { resource_klass.new(labels) }

  describe '#create_editor_be' do
    it 'retrieves team members from www-gitlab-com and returns a random create editor backend engineer' do
      allow(WwwGitLabCom).to receive(:team_from_www).and_return(team_from_www)

      expect(%w(@user1 @user3)).to include(subject.create_editor_be)
    end
  end

  describe '#create_editor_fe' do
    it 'retrieves team members from www-gitlab-com and returns a random create editor frontend engineer' do
      allow(WwwGitLabCom).to receive(:team_from_www).and_return(team_from_www)

      expect(%w(@user2 @user4)).to include(subject.create_editor_fe)
    end
  end
end
