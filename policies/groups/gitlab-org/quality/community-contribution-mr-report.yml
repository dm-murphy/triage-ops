resource_rules:
  merge_requests:
    summaries:
      - name: Identify idle and recently merged ~"Community contribution" merge requests
        actions:
          summarize:
            destination: gitlab-org/quality/triage-reports
            title: |
              #{Date.today.iso8601} - Community contributions report
            summary: |
              This report contains two lists of community contributions:

              1. Merge requests from first-time contributors that have not had a human interaction in the past 7 days.
              1. Merge requests that are idle (28 days without human interaction), probably awaiting a response from GitLab team members.

              Assignees should go through both lists and process the merge requests in them. All merge requests require a response. After processing a merge request, the checkbox on the left should be checked.

              When going through the list of merge requests, please do one of the following:

              - If a reviewer is assigned and the last comment is from the reviewer, ping the author in a comment and set ~"workflow::in dev".
              - If a reviewer is assigned and the last comment is from the author, ping the reviewer in a comment, and set ~"workflow::ready for review".
              - If no reviewer is assigned, use https://gitlab-org.gitlab.io/gitlab-roulette/ or https://about.gitlab.com/handbook/product/product-categories/ to find a reviewer and assign the merge request to them.
              - If the merge request depends on the completion of work elsewhere, set ~"workflow::blocked".

              Overview of merge requests:

              - In dev (author should get back to us) - https://gitlab.com/groups/gitlab-org/-/merge_requests?scope=all&state=opened&label_name[]=Community%20contribution&label_name[]=workflow%3A%3Ain%3A%3Adev
              - Ready for review (a team member should pick the review) - https://gitlab.com/groups/gitlab-org/-/merge_requests?scope=all&state=opened&label_name[]=Community%20contribution&label_name[]=workflow%3A%3Aready%3A%3Afor%3A%3Areview
              - In review (a team member is actively reviewing) - https://gitlab.com/groups/gitlab-org/-/merge_requests?scope=all&state=opened&label_name[]=Community%20contribution&label_name[]=workflow%3A%3Ain%3A%3Areview
              - Blocked (inactionable) - https://gitlab.com/groups/gitlab-org/-/merge_requests?scope=all&state=opened&label_name[]=Community%20contribution&label_name[]=workflow%3A%3Ablocked

              For more info about the process, see https://about.gitlab.com/handbook/engineering/quality/contributor-success/#working-with-community-contributions.

              #{ short_team_summary(title: "## Community Contribution Report", **group_contributor_success) }

              ----

              Job URL: #{ENV['CI_JOB_URL']}

              *This report was generated from [this policy](#{ENV['CI_PROJECT_URL']}/blob/#{ENV['CI_DEFAULT_BRANCH']}/policies/groups/gitlab-org/quality/community-contribution-mr-report.yml).*
        rules:
          - name: 1. First-time contributor merge requests
            conditions:
              state: opened
              labels:
                - Community contribution
                - 1st contribution
              forbidden_labels:
                - idle
                - stale
                - "workflow::in dev" # author is working on the issue
                - "workflow::in review"
                - "workflow::blocked"
              ruby: |
                IdleMrHelper.idle?(resource, days: 7)
            actions:
              summarize:
                item: |
                  - [ ] #{IdleMrHelper.days_since_last_human_update(resource)} days since last update | #{resource[:web_url]}+ | {{labels}}
                summary: |
                  ### #{resource[:items].lines.size} ~"1st contribution" Merge Requests

                  {{items}}
          - name: 2. Idle ~"Community Contribution" merge requests awaiting GitLab response
            conditions:
              state: opened
              labels:
                - Community contribution
                - idle
              forbidden_labels:
                - "workflow::in dev" # author is working on the issue
                - "workflow::blocked"
            actions:
              summarize:
                item: |
                  - [ ] #{IdleMrHelper.days_since_last_human_update(resource)} days since last update | #{resource[:web_url]}+ | {{labels}}
                summary: |
                  ### #{resource[:items].lines.size} Idle (28 days without human interaction) merge requests awaiting GitLab response

                  {{items}}
          - name: 3. Merged ~"Community contribution" merge requests
            api: graphql
            conditions:
              state: merged
              labels:
                - Community contribution
              date:
                attribute: merged_at
                condition: newer_than
                interval_type: days
                interval: 7
            actions:
              summarize:
                item: |
                  `{{author}}`,#{resource[:web_url]},{{labels}}
                summary: |
                  #{ mrs = merge_requests_from_line_items(resource[:items]); nil }
                  #{ author_count = mrs.group_by { |mr| mr[:author] }.count; nil }

                  **#{mrs.count}** ~"Community contribution" merge requests from **#{author_count} unique authors** merged in the past 7 days
