# Running locally

```shell
{
  # Removes older containers
  docker rm `docker ps -aq`

  # Builds the docker image named `triage-ops`
  docker build --file=Dockerfile.rack -t triage-ops .

  # Run the server locally (in dry mode)
  docker run -itp 8091:8080 \
    -e GITLAB_API_ENDPOINT=http://host.docker.internal:3000/api/v4 \
    -e GITLAB_WEBHOOK_TOKEN=<WEBHOOK_TOKEN_PRINTED_IN_RAKE_TASK_ABOVE> \
    -e GITLAB_API_TOKEN=<API_TOKEN_PRINTED_IN_RAKE_TASK_ABOVE> \
    -e SLACK_WEBHOOK_URL=https://example.org \
    triage-ops
}
```

You can now test the endpoint:

```shell
curl -X POST 0.0.0.0:8091 -H "Content-Type: application/json" -H "X-Gitlab-Token: gitlab_webhook_token" -d @spec/fixtures/reactive/gitlab_test_note.json
```

## Running with a local GitLab instance

You can run `triage-ops` with a local GitLab instance (e.g GDK) and have it react to events coming from the local instance.

To load all prerequisites for your local GitLab instance, use a
[Triage Ops seed file](https://gitlab.com/gitlab-org/gitlab/-/blob/master/db/fixtures/development/33_triage_ops.rb)
by running this command in the GitLab Rails project directory:

```shell
cd <local-gitlab-instance-folder>
bundle exec rake db:seed_fu FILTER=33_triage_ops SEED_TRIAGE_OPS=true
```

This command:

- Generates a seed file.
- Creates and prints a new `GITLAB_API_TOKEN` and `GITLAB_WEBHOOK_TOKEN`.

Once the prerequisites are all set, you can start the server locally as shown in the [Running locally](#running-locally) section, but not in dry-run mode this time:

```shell
# Docker command
# 
# Note: You probably should not use docker desktop at GitLab.
# See https://about.gitlab.com/handbook/tools-and-tips/mac/#docker-desktop for more info
# 
# Also, host.docker.internal is for macOS hosts only. For linux, you might want to use 172.17.0.1
#
# (source: https://stackoverflow.com/questions/48546124/what-is-linux-equivalent-of-host-docker-internal)
{
  docker rm `docker ps -aq`
  docker build --file=Dockerfile.rack -t triage-ops .
  docker run -itp 8091:8080 \
    -e GITLAB_API_ENDPOINT=http://host.docker.internal:3000/api/v4 \
    -e GITLAB_WEBHOOK_TOKEN=<WEBHOOK_TOKEN_PRINTED_IN_RAKE_TASK_ABOVE> \
    -e GITLAB_API_TOKEN=<API_TOKEN_PRINTED_IN_RAKE_TASK_ABOVE> \
    -e SLACK_WEBHOOK_URL=https://example.org \
    triage-ops
}

# https://about.gitlab.com/handbook/tools-and-tips/mac/#docker-desktop
# 
# nerdctl command
# 
# The IP 192.168.5.2 comes from https://github.com/lima-vm/lima/blob/master/docs/network.md#host-ip-19216852
{
  docker rm `docker ps -aq`
  docker build --file=Dockerfile.rack -t triage-ops .
  docker run -itp 8091:8080 \
  -e GITLAB_API_ENDPOINT=http://192.168.5.2:3000/api/v4 \
  -e GITLAB_WEBHOOK_TOKEN=<WEBHOOK_TOKEN_PRINTED_IN_RAKE_TASK_ABOVE> \
  -e GITLAB_API_TOKEN=<API_TOKEN_PRINTED_IN_RAKE_TASK_ABOVE> \
  -e SLACK_WEBHOOK_URL=https://example.org \
  triage-ops
}
```

TODO:
- [ ] Stub Slack webhook URL
